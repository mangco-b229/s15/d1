// JS COMMENTS
// they are important as a guide for the programmer.
// Comments are disrecarded no matter how long it may be.
	// Multi-Line Comments ctrl+shift+/


//STATEMENTS
//programming instruction that we tell the computer to perform
//Statements Usually ends with a semicolon ( ; )

//SYNTAX 
//It is the set of rules that describes how statements must be constructed


// alert("HELLO AGAIN");	

//this is a statement with a correct syntax
console.log('Hello World');

//JS is a loosetype type programming lnaguage 

console. log ( " Hellow World!" ) ;


// and also with this

console.
log
(
"Hellow Againn"

)

// [SECTION] VARIABLES
// it is used as a container or storage


// Declaring Variables - tells our device hat a variable name is created and is ready to store date


//DIFFERENT CASING STYLES
// cammel case -> thisIsCammelCasing;
// snake case -> this_is_snake_casing;
// kebab case -> this-is-kebab-casing;

//LET --> is a keyword that is usually used to declare a variable

let myVariable;
let hello;

console.log(myVariable);
console.log(hello);


//this 
let firstName = "Rick";

let pokemon = 1200;

// let/const variableName = value;

let productName = "desktop computer";
console.log(productName);

let productPrice = 18199;
console.log(productPrice);


productPrice = 3000
console.log(productPrice);

let friend = "kate";


//NUMBERS

let headcount = 26;
console.log(headcount);


let grade = 98.7;
console.log(grade)

let grades = [98.7 , 90.8 , 88.5];


//objects
//hold properties that describes the variable
//syntax --> let/const objectName - {propertyA: value, propertyB: value}


let person = {
	name: "Juan dela Cruz",
	age: 89,
	contacts: ["1241241", "1241212", "1241241"],
	address: {
		houseNumber: "355",
		city: "Manila"
	}
}

console.log(person)


let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
}


const anime = ["One Piece" , "Attack on titan", "Kimetsu no Yaiba"];


anime[0] = "Bleach";
console.log(anime);

let spouse = null;

